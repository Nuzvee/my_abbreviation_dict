

import tkinter as tk 
from tkinter.messagebox import showinfo  
root=tk.Tk() 
  
root.geometry("600x400") 

def import_dict():
    mydict = {}
    with open ("abbreviations_dict.txt", "r") as r:
        for i in r:
            if i.strip() == "":
                pass
            else:
                i = i.strip()
                i = i.split(" - ")
                key = i[0].strip().upper()
                value = i[1].strip()
                
                # if key exists more than once in dict with different value, append values to single key
                if key not in mydict:
                    mydict[key] = value
                elif isinstance(mydict[key], list):
                    mydict[key].append(value)
                else:
                    mydict[key] = [mydict[key], value]
                   
    #with open ("C:\playhouse\Cisco_troubleshooting_commands_DCIT.txt", "r") as r:
    
    return mydict

def bad_solution(event=None):
    
    retval_1 = submit()
    retval_2 = submit_2()
    
    retval = f"{retval_1}, {retval_2}"
    return retval


def submit(event=None): 
    mydict = import_dict()
    name=name_entry.get().strip()
    
    if name.replace(" ", "") == "":
        name_entry.delete(0,999)
        return
    try:
        retval = mydict[name.upper()]
    except:
        retval = "Abbreviation not found"
    if type(retval) == list:
        retval_temp = ""
        printval = ""
        retList_temp = []
        for i in retval:
            if retval_temp == "":
                retval_temp = i
                printval = f"- {i}\n"
                retList_temp.append(i)
            else:
                skipstr = False
                for u in retList_temp:
                    if i == u:
                        skipstr = True
                if skipstr is False:
                    retval_temp = f"{retval_temp},\n{i}"
                    printval = f"{printval}\n- {i}\n"
                    retList_temp.append(i)
        retval = retval_temp
    else:
        printval = retval
    showinfo(title = name.upper(), message = printval)#retval)
    print(f"{name.upper()} - {retval}")
    name_entry.delete(0,999)
    return retval

"""
def submit_2(event=None): 
    name=name_entry_2.get().strip()
    
    if name.replace(" ", "") == "":
        name_entry_2.delete(0,999)
        return
    try:
        if :
            second_octet = 
            third_octet = 
        else:
            second_octet = 
            third_octet = 
        retval = f"{name} => 10.{second_octet}.{third_octet}.0"
    except:
        retval = "whut?"

    printval = retval
    showinfo(title = name.upper(), message = printval)#retval)
    print(f"{name.upper()} - {retval}")
    name_entry_2.delete(0,999)
    return retval
"""    
name_label = tk.Label(root, text = 'Abbreviation:', 
                      font=('calibre', 
                            20, 'bold')) 
    
name_entry = tk.Entry(root, 
                      font=('calibre',20,'normal')) 
                      
name_label_2 = tk.Label(root, text = 'cluster Calc:', 
                      font=('calibre', 
                            10, 'bold')) 
    
name_entry_2 = tk.Entry(root, 
                      font=('calibre',10,'normal'))     


nope_1 = tk.Label(root, text = '', 
                      font=('calibre', 
                            20, 'bold')) 
nope_2 = tk.Label(root, text = '', 
                      font=('calibre', 
                            20, 'bold')) 
nope_3 = tk.Label(root, text = '', 
                      font=('calibre', 
                            20, 'bold')) 
nope_4 = tk.Label(root, text = '', 
                      font=('calibre', 
                            20, 'bold'))                             
"""
Lb1 = tk.Listbox(root)
Lb1.insert(1, "Python")
Lb1.insert(2, "Perl")
Lb1.insert(3, "C")
Lb1.insert(4, "PHP")
Lb1.insert(5, "JSP")
Lb1.insert(6, "Ruby")

#for key in d:
#    lstbox.insert(END, '{}: {}'.format(key, d[key]))
#Lb1.pack()
"""
   
#root.bind('<Return>', submit)
root.bind('<Return>', bad_solution)
sub_btn=tk.Button(root,text = 'Submit', 
                  command = submit) 
                  
#sub_btn_2=tk.Button(root,text = 'Submit', 
#                  command = submit_2) 

name_label.grid(row=0,column=0) 
name_entry.grid(row=0,column=1)  
sub_btn.grid(row=2,column=1) 

nope_1.grid(row=3,column=1)
nope_2.grid(row=4,column=1)
nope_3.grid(row=5,column=1)
nope_4.grid(row=6,column=1)
#name_label_2.grid(row=7,column=0) 
#name_entry_2.grid(row=7,column=1)  
#sub_btn_2.grid(row=9,column=1) 
#Lb1.grid(row=1,column=3)
root.mainloop() 